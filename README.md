**Simple graph library**
---

## Supports directed and undirected graphs 

1. addVertex - adds vertex to the graph
2. addEdge - adds edge to the graph
3. getPath - returns a list of edges between 2 vertices