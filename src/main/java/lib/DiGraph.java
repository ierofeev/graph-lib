package lib;

import lib.utils.GraphUtils;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
public class DiGraph extends AbstractGraph {

    DiGraph(Vertex vertex) {
        addVertex(vertex);
    }

    @Override
    public boolean isDirect() {
        return true;
    }

    @Override
    public void addEdge(@NonNull String first, @NonNull String second) {
        Edge edge = GraphUtils.getEdge(this, first, second);

        getGraph().get(edge.getV1()).add(edge.getV2());
    }

    @Override
    public Set<Vertex> getPath(String first, String second) {
        Edge edge = GraphUtils.getEdge(this, first, second);
        Vertex v1 = edge.getV1();
        Vertex v2 = edge.getV2();

        return GraphUtils.depthFirstTraverse(this, v1, v2);
    }
}
