package lib.exception;

public class NoSuchVertexException extends IllegalArgumentException {
    public NoSuchVertexException() {
        super("No such vertex in the graph");
    }
}
