package lib.exception;

public class IllegalGraphTypeException extends IllegalArgumentException {
    public IllegalGraphTypeException() {
        super("Unknown graph type");
    }
}
