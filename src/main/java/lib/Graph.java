package lib;

import java.util.Set;

public interface Graph {

    /**
     * Is current graph direct or undirected
     *
     * @return boolean value
     */
    boolean isDirect();

    /**
     * Adds vertex to the graph
     *
     * @param name - Vertex name
     */
    void addVertex(String name);

    /**
     * Adds edge to the graph
     *
     * @param first - name of the first Vertex
     * @param second - name of the second Vertex
     */
    void addEdge(String first, String second);

    /**
     * Return path from one Vertex to other
     *
     * @param start - name of the start Vertex
     * @param end - name of the end Vertex
     * @return Set<Vertex>
     */
    Set<Vertex> getPath(String start, String end);
}
