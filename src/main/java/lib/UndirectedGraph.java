package lib;

import lib.utils.GraphUtils;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
public class UndirectedGraph extends AbstractGraph {

    UndirectedGraph(Vertex vertex) {
        addVertex(vertex);
    }

    @Override
    public boolean isDirect() {
        return false;
    }

    @Override
    public void addEdge(@NonNull String first, @NonNull String second) {
        Edge edge = GraphUtils.getEdge(this, first, second);
        Vertex v1 = edge.getV1();
        Vertex v2 = edge.getV2();

        getGraph().get(v1).add(v2);
        getGraph().get(v2).add(v1);
    }

    @Override
    public Set<Vertex> getPath(String first, String second) {
        Edge edge = GraphUtils.getEdge(this, first, second);
        Vertex v1 = edge.getV1();
        Vertex v2 = edge.getV2();

        return GraphUtils.depthFirstTraverse(this, v1, v2);
    }
}
