package lib.utils;

import lib.*;
import lib.exception.IllegalGraphTypeException;
import lib.exception.NoSuchVertexException;

import java.util.*;

public class GraphUtils {

    private static <T extends Graph> Set<Vertex> getAdjacentVertices(T graph, Vertex root) {
        if (graph instanceof WeightGraph) {
            Set<Vertex> adj = new HashSet<>();
            ((WeightGraph) graph).getGraph().get(root).forEach(edge -> adj.add(edge.getV2()));
            return adj;
        } else if (graph instanceof AbstractGraph) {
            return ((AbstractGraph) graph).getGraph().get(root);
        } else {
            throw new IllegalGraphTypeException();
        }
    }

    public static Set<Vertex> depthFirstTraverse(Graph graph, Vertex root, Vertex destination) {
        Stack<Vertex> stack = new Stack<>();
        Set<Vertex> visited = new HashSet<>();
        stack.push(root);
        while (!stack.isEmpty() && !stack.contains(destination)) {
            Vertex vertex = stack.pop();
            if (!visited.contains(vertex)) {
                getAdjacentVertices(graph, vertex).forEach(stack::push);
                visited.add(vertex);
            }
        }

        return visited;
    }

    public static Set<Vertex> breadthFirstTraverse(Graph graph, Vertex root, Vertex destination) {
        Set<Vertex> visited = new HashSet<>();
        Queue<Vertex> queue = new ArrayDeque<>();
        queue.add(root);
        visited.add(root);
        while (!queue.isEmpty() && !queue.contains(destination)) {
            Vertex vertex = queue.poll();
            getAdjacentVertices(graph, vertex).stream()
                    .filter(v -> !visited.contains(v))
                    .forEach(v -> {
                        queue.add(v);
                        visited.add(v);
                    });

        }

        return visited;
    }

    public static <T extends Graph> Edge getEdge(T graph, String first, String second) {
        Set<Vertex> vertexes = null;
        if (graph instanceof WeightGraph) {
            vertexes = ((WeightGraph) graph).getGraph().keySet();
        } else if (graph instanceof AbstractGraph) {
            vertexes = ((AbstractGraph) graph).getGraph().keySet();
        } else {
            throw new IllegalGraphTypeException();
        }
        Vertex v1 = null, v2 = null, iterableVertex;
        Iterator<Vertex> iterator = vertexes.iterator();

        while ((v1 == null || v2 == null) && iterator.hasNext()) {
            iterableVertex = iterator.next();
            if (first.equals(iterableVertex.getName())) {
                v1 = iterableVertex;
            }
            if (second.equals(iterableVertex.getName())) {
                v2 = iterableVertex;
            }
        }

        if (v1 == null || v2 == null) {
            throw new NoSuchVertexException();
        }

        return new Edge(v1, v2);
    }
}
