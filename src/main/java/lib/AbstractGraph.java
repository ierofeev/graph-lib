package lib;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@RequiredArgsConstructor
public abstract class AbstractGraph implements Graph {

    @Getter
    private final Map<Vertex, Set<Vertex>> graph = new ConcurrentHashMap<>();

    @Override
    public void addVertex(String name) {
        graph.putIfAbsent(new Vertex(name), Collections.synchronizedSet(new HashSet<>()));
    }

    public void addVertex(Vertex vertex) {
        graph.putIfAbsent(vertex, Collections.synchronizedSet(new HashSet<>()));
    }

    public abstract void addEdge(String first, String second);

    public abstract boolean isDirect();

    public abstract Set<Vertex> getPath(String start, String end);
}
