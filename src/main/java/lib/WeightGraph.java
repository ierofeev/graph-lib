package lib;

import lib.utils.GraphUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@RequiredArgsConstructor
public class WeightGraph implements Graph {

    @Getter
    private final Map<Vertex, Set<Edge>> graph = new ConcurrentHashMap<>();

    @Override
    public boolean isDirect() {
        return false;
    }

    @Override
    public void addVertex(String name) {
        graph.putIfAbsent(new Vertex(name), Collections.synchronizedSet(new HashSet<>()));
    }

    @Override
    public void addEdge(String first, String second) {
        Edge edge = GraphUtils.getEdge(this, first, second);

        graph.get(edge.getV1()).add(edge);
    }

    public void addEdge(String first, String second, double weight) {
        Edge edge = GraphUtils.getEdge(this, first, second);
        edge.setWeight(weight);

        graph.get(edge.getV1()).add(edge);
    }

    @Override
    public Set<Vertex> getPath(String first, String second) {
        Edge edge = GraphUtils.getEdge(this, first, second);
        Vertex v1 = edge.getV1();
        Vertex v2 = edge.getV2();

        return GraphUtils.depthFirstTraverse(this, v1, v2);
    }

}
