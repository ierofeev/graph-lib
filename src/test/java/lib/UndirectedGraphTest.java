package lib;

import lib.exception.NoSuchVertexException;
import lib.utils.GraphUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class UndirectedGraphTest {

    private final String rootName = "A";
    private final Vertex root = new Vertex(rootName);

    @Test
    void createUndirectedGraphTest() {
        UndirectedGraph undirectedGraph = new UndirectedGraph(root);

        assertNotNull(undirectedGraph);
    }

    @Test
    void isDirectTest() {
        UndirectedGraph undirectedGraph = new UndirectedGraph(root);
        assertFalse(undirectedGraph.isDirect());
    }

    @Test
    void addVertexTest() {
        String v1Name = "B";
        Vertex v1 = new Vertex(v1Name);

        UndirectedGraph undirectedGraph = new UndirectedGraph(root);
        undirectedGraph.addVertex(v1Name);

        assertNotNull(undirectedGraph.getGraph());
        assertNotNull(undirectedGraph.getGraph().get(v1));
        assertEquals(2, undirectedGraph.getGraph().keySet().size());
    }

    @Test
    void addEdgeTest() {
        UndirectedGraph undirectedGraph = new UndirectedGraph(root);
        String v1Name = "A";
        String v2Name = "B";

        final Executable executable = () -> undirectedGraph.addEdge(v1Name, v2Name);
        assertThrows(NoSuchVertexException.class, executable);
        undirectedGraph.addVertex(v1Name);
        assertThrows(NoSuchVertexException.class, executable);

        undirectedGraph.addVertex(v2Name);
        undirectedGraph.addEdge(v1Name, v2Name);
        assertNotNull(GraphUtils.getEdge(undirectedGraph, v1Name, v2Name));
    }

    @Test
    void getPathTest() {
        UndirectedGraph undirectedGraph = new UndirectedGraph(root);

        Vertex v1 = new Vertex("V1");
        Vertex v2 = new Vertex("V2");
        Vertex v3 = new Vertex("V3");
        Vertex v4 = new Vertex("V4");

        undirectedGraph.addVertex(v1);
        undirectedGraph.addVertex(v2);
        undirectedGraph.addVertex(v3);
        undirectedGraph.addVertex(v4);

        undirectedGraph.addEdge("V1", "V2");
        undirectedGraph.addEdge("V1", "V3");
        undirectedGraph.addEdge("V3", "V4");

        Set<Vertex> path = undirectedGraph.getPath("V1", "V4");
        assertNotNull(path);
        assertEquals(2, path.size());
        Iterator<Vertex> pathIterator = path.iterator();
        assertEquals("V1", pathIterator.next().getName());
        assertEquals("V3", pathIterator.next().getName());

        Set<Vertex> reversePath = undirectedGraph.getPath("V4", "V1");
        assertNotNull(reversePath);
        assertEquals(2, reversePath.size());
        Iterator<Vertex> reversePathIterator = reversePath.iterator();
        assertEquals("V3", reversePathIterator.next().getName());
        assertEquals("V4", reversePathIterator.next().getName());
    }
}