package lib;

import lib.exception.NoSuchVertexException;
import lib.utils.GraphUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DiGraphTest {

    private final String rootName = "A";
    private final Vertex root = new Vertex(rootName);

    @Test
    void createDirectedGraphTest() {
        DiGraph directedGraph = new DiGraph(root);

        assertNotNull(directedGraph.getGraph().get(root));
    }

    @Test
    void addVertexTest() {
        String v1Name = "B";
        Vertex v1 = new Vertex(v1Name);

        DiGraph directedGraph = new DiGraph(root);
        directedGraph.addVertex(v1Name);

        assertNotNull(directedGraph.getGraph());
        assertNotNull(directedGraph.getGraph().get(v1));
        assertEquals(2, directedGraph.getGraph().keySet().size());
    }

    @Test
    void addEdgeTest() {
        DiGraph directedGraph = new DiGraph(root);
        String v1Name = "A";
        String v2Name = "B";

        final Executable executable = () -> directedGraph.addEdge(v1Name, v2Name);
        assertThrows(NoSuchVertexException.class, executable);
        directedGraph.addVertex(v1Name);
        assertThrows(NoSuchVertexException.class, executable);

        directedGraph.addVertex(v2Name);
        directedGraph.addEdge(v1Name, v2Name);
        assertNotNull(GraphUtils.getEdge(directedGraph, v1Name, v2Name));
    }

    @Test
    void isDirectTest() {
        DiGraph directedGraph = new DiGraph(root);
        assertTrue(directedGraph.isDirect());
    }

    @Test
    void getPathTest() {
        DiGraph directedGraph = new DiGraph(root);

        Vertex v1 = new Vertex("V1");
        Vertex v2 = new Vertex("V2");
        Vertex v3 = new Vertex("V3");
        Vertex v4 = new Vertex("V4");

        directedGraph.addVertex(v1);
        directedGraph.addVertex(v2);
        directedGraph.addVertex(v3);
        directedGraph.addVertex(v4);

        directedGraph.addEdge("V1", "V2");
        directedGraph.addEdge("V1", "V3");
        directedGraph.addEdge("V3", "V4");

        Set<Vertex> path = directedGraph.getPath("V1", "V4");
        assertNotNull(path);
        assertEquals(2, path.size());
        Iterator<Vertex> pathIterator = path.iterator();
        assertEquals("V1", pathIterator.next().getName());
        assertEquals("V3", pathIterator.next().getName());

        Set<Vertex> reversePath = directedGraph.getPath("V4", "V1");
        assertNotNull(reversePath);
        assertEquals(1, reversePath.size());
        assertEquals("V4", reversePath.iterator().next().getName());
    }
}